<?php

class last_comments {

    // Marker for plugin
    private $DB;

    public function __construct($params) {
        $this->DB = getDB();
    }

    public function common($params) {

        $config = json_decode(file_get_contents(dirname(__FILE__).'/config.json'), true);

        $Cache = new \Cache;
        $Cache->lifeTime = 600;
        if ($Cache->check('pl_last_comments')) {
            $comments = $Cache->read('pl_last_comments');
            $comments = unserialize($comments);
        } else {
            $sql = "(SELECT a.`date`, a.`id`, a.`entity_id`, a.`name`, a.`user_id`, a.`message`, b.`title`, b.`hlu`, a.`module`
                FROM `" . $this->DB->getFullTableName('comments') . "` a 
                JOIN `" . $this->DB->getFullTableName('news') . "` b ON b.`id` = a.`entity_id` WHERE a.`module` = 'news')
                UNION (SELECT a.`date`, a.`id`, a.`entity_id`, a.`name`, a.`user_id`, a.`message`, b.`title`, b.`hlu`, a.`module`
                FROM `" . $this->DB->getFullTableName('comments') . "` a 
                JOIN `" . $this->DB->getFullTableName('stat') . "` b ON b.`id` = a.`entity_id` WHERE a.`module` = 'stat')
                UNION (SELECT a.`date`, a.`id`, a.`entity_id`, a.`name`, a.`user_id`, a.`message`, b.`title`, b.`hlu`, a.`module`
                FROM `" . $this->DB->getFullTableName('comments') . "` a 
                JOIN `" . $this->DB->getFullTableName('loads') . "` b ON b.`id` = a.`entity_id` WHERE a.`module` = 'loads')
                UNION (SELECT a.`date`, a.`id`, a.`entity_id`, a.`name`, a.`user_id`, a.`message`, b.`title`, b.`title` as 'hlu', a.`module`
                FROM `" . $this->DB->getFullTableName('comments') . "` a 
                JOIN `" . $this->DB->getFullTableName('foto') . "` b ON b.`id` = a.`entity_id` WHERE a.`module` = 'foto')
                ORDER BY `date` DESC LIMIT ". $config['limit'] ." ";
            $comments = $this->DB->query($sql);
            $Cache->write(serialize($comments), 'pl_last_comments', array());
        }

        if (!empty($comments)) {
            $i = 0;
            foreach ($comments as $key => $comm) {
                $str = 'к материалу';
                    switch ($comm['module']) {
                        case 'foto': $str = 'к фотографии'; break;
                        case 'loads': $str = 'к загрузке'; break;
                        case 'news': $str = 'к новости'; break;
                        case 'stat': $str = 'к статье'; break;
                    }

                $i++;

                $unixtime = strtotime($comm['date']);
                $year = date('Y', $unixtime);
                $month = date('m', $unixtime);
                $day = date('d', $unixtime);

                $comments[$key]['module'] = $str;
                $comments[$key]['number'] = $i;
                $comments[$key]['title'] = h($comm['title']);
                $comments[$key]['date'] = $comm['date'];

                if ($comm['module'] == 'foto') {
                    $comments[$key]['url'] = get_url(Register::getClass('AtmUrl')->getEntryUrl(
                        $comm['entity_id'], $comm['module']
                    ));
                } else {
                    $comments[$key]['url'] = get_url(Register::getClass('AtmUrl')->getEntryUrl(
                        $comm['entity_id'], $comm['module'], $comm['hlu'], $year, $month, $day
                    ));
                }
                $comments[$key]['name'] = $comm['name'];
                $comments[$key]['avatar'] = getAvatar($comm['user_id']);
                $comm_text = $comm['message'];
                $comm_text = Register::getClass('PrintText')->getAnnounce($comm_text, $comments[$key]['url'], $config['shot_comm']);
                $comments[$key]['message'] = $comm_text;
            }
        }
        $params['plugin_last_comments'] = $comments;
        return $params;
    }
}
