## Плагин "Последние комментарии"

Версия: 0.3
Лицензия GNU GPL

***

Выводит последние комментарии из материалов в метку {{ last_comments }}

## Установка:

1. Распаковать архив и переместить папку last_comments в /plugins.
2. Для файлов /plugins/last_comments/config.json и /plugins/last_comments/template/comments.html выставить права 777.
3. Добавить вызов плагина в нужном месте шаблона:

    {% if atm.plugin_last_comments %}
        {% include '@last_comments/list.html.twig' %}
    {% endif %}
